from django.urls import path
from . import views

urlpatterns = [
    path('', views.PostView.as_view()),
    path('<int:pk>/', views.PostDetail.as_view()),
    path('review/<int:pk>/', views.AddComments.as_view(), name='add_comments'),
    path('<int:pk>/add_likes/', views.AddLike.as_view(), name='add_likes'),
    path('fotoalbum/', views.Foto.as_view(), name='fotoalbum'),
    path('about_my/', views.AboutMy.as_view(), name='about_my')
]
